# **Formatos**
========

## CSV
*Comma Separated Values*

Tipo de documento en formato abierto, para representar datos en forma de tabla, 
en la que las columnas se separan por comas y las filas por saltos de línea 
[Wikipedia] (https://es.wikipedia.org/wiki/Valores_separados_por_comas) 

### TIFF 
*Tagged Image Filed Format*

Es un formato de archivo informático para almacenar imágenes de mapa de bits. 
Es prevalente en la industria gráfica y en la fotográfia profesional por su 
versatilidad y comprensión no destructiva. [Wikipedia] 
(https://es.wikipedia.org/wiki/TIFF)

#### JPEG
*Joint Photographic Experts Group*

Nombre de un comité de expertos que creó un estándar de comprensión y 
codificación de archivos e imágenes fijas. A menudo considerado como un formato 
de archivo. [Wikipedia] 
(https://es.wikipedia.org/wiki/Joint_Photographic_Experts_Group)
